package com.mycompany.singleton_jave;
class ConfigManager {
    private static ConfigManager instance;
    private String configFile;

    private ConfigManager() {
        configFile = "config.properties";
        // Load configuration settings from file or other sources
    }

    public static ConfigManager getInstance() {
        if (instance == null) {
            instance = new ConfigManager();
        }
        return instance;
    }

    public String getConfig(String key) {
        // Retrieve configuration setting based on the key
        return "Value for " + key;
    }
}
public class Singleton_jave {

    public static void main(String[] args) {
        ConfigManager configManager = ConfigManager.getInstance();

        String value1 = configManager.getConfig("setting1");
        String value2 = configManager.getConfig("setting2");

        System.out.println("Setting 1: " + value1);
        System.out.println("Setting 2: " + value2);

        // Since it's a Singleton, you get the same instance no matter how many times you call getInstance()
        ConfigManager configManagerAgain = ConfigManager.getInstance();

        System.out.println(configManager == configManagerAgain); // Output: true - Both instances are the same
    }
}
